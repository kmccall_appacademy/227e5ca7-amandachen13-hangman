class Hangman
  attr_reader :guesser, :referee, :board

  MAX_GUESSES = 10

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @remaining_guesses = MAX_GUESSES
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = [nil] * length
  end

  def play
    self.setup
    while @remaining_guesses > 0
        self.display_board
        self.take_turn
        @remaining_guesses -= 1
        if won?
          puts "Guesser wins!"
          return
        end
    end
    puts "Guesser loses!"
    puts "Word was: #{referee.secret_word}"
  end

  def take_turn
    letter = @guesser.guess
    indices = @referee.check_guess(letter)
    self.update_board(letter, indices)
    @guesser.handle_response(letter, indices)
  end

  def update_board(letter, indices)
     indices.each do |idx|
       @board[idx] = letter
     end
  end

  def display_board
    puts "#{@board}"
  end

  def won?
    @board.include?(nil) == false
  end

end

class HumanPlayer

  def initialize(name)
    @name = name
  end

  def guess
    puts "Guess a letter: "
    gets.chomp
  end

  def register_secret_length(length)
    puts "The length of the secret word is #{length}."
  end

  def handle_response(letter, indices)
  end

end

class ComputerPlayer
  attr_reader :dictionary, :secret_word, :candidate_words

  def initialize(dictionary = File.readlines("dictionary.txt"))
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select {|word| word.length == length }
  end

  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index do |ch, idx|
      indices << idx if ch == letter
    end
    indices
  end

  def check_word(word, letter)
    indices = []
    word.chars.each_with_index do |ch, idx|
      indices << idx if ch == letter
    end
    indices
  end

  def handle_response(letter, indices)
    @candidate_words.select! do |word|
      self.check_word(word, letter) == indices
    end
  end

  def guess(board)
    letters = @candidate_words.join.chars
    letters_freq = Hash.new(0)
    letters.each do |letter|
      letters_freq[letter] += 1
    end
    board.each {|letter| letters_freq.delete(letter)}
    guess = letters_freq.sort_by {|_, v| v}.last.first
  end

end

if __FILE__ == $PROGRAM_NAME
  guesser = HumanPlayer.new("Amanda")
  referee = ComputerPlayer.new
  Hangman.new({guesser: guesser, referee: referee}).play
end
